export const cz = {
  search: {
    no_result: "Nebyl nalezen žádný výsledek!",
    button: "Hledej",
    title: "Moje knihovna",
    placeholder: "zadej název knihy nebo jméno autora",
  },
  login: {
    title: "Přihlaste se ke svému účtu",
    button: "Přihlasit",
  }
}
