export const sk = {
  search: {
    no_result: "Nenašli sa žiadne výsledky!",
    button: "Hľadaj",
    title: "Moja knižnica",
    placeholder: "zadaj názov knihy alebo meno autora",
  },
  login: {
    title: "Prihláste sa do svojho účtu",
    button: "Prihlásiť",
  }
}
