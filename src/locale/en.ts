export const en = {
  search: {
    no_result: "No result found!",
    button: "Search",
    title: "My Library",
    placeholder: "enter a book title or author name",
  },
  login: {
    title: "Login to your account",
    button: "Login",
  }
}
