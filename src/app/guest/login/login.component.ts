import { Component, OnInit } from '@angular/core';
import {BookService} from "../../services/book.service";
import {GlobalStorageService} from "../../services/global-storage.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public login: string = "admin@admin.sk";
  public password: string = "peter";
  public loginError = false;
  public emptyInputError = false;

  constructor(
    private bookService: BookService,
    private globalStorageService: GlobalStorageService,
    private router: Router
  ) { }

  ngOnInit(): void {
  }

  loginUser(){
    this.loginError = false;
    this.emptyInputError = false;
    if (!this.login || !this.password) {
      this.emptyInputError = true;
    } else {
      this.bookService.login(this.login, this.password).subscribe(
        (token) =>{
          if (token) {
            this.saveToken(token)
            this.redirectToAdmin();
          }
          this.loginError = false;
        },
        (error) =>{
          this.loginError = true;
          console.log("Something went wrong");
          console.log(error);
        }
      );
    }
  }

  redirectToAdmin() {
    this.router.navigate(["/admin"]);
  }

  saveToken(token: string) {
    this.globalStorageService.login(token);
  }
}
