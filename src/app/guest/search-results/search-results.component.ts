import { Component, OnInit } from '@angular/core';
import {BookService} from "../../services/book.service";
import {Book} from "../../models/Book";
import {GlobalStorageService} from "../../services/global-storage.service";

@Component({
  selector: 'app-search-results',
  templateUrl: './search-results.component.html',
  styleUrls: ['./search-results.component.css']
})
export class SearchResultsComponent implements OnInit {
  public query = ""
  public books: Book[];

  constructor(
    private bookService: BookService,
    public globalStorageService: GlobalStorageService
  ) { }

  ngOnInit(): void {
  }

  search() {
    console.log("Searching " + this.query + "...")
    this.bookService.getBooks(this.query).subscribe(
      (booksFromService) => {
        console.log(booksFromService)
        this.books = booksFromService;
      }
    )
  }

}
