import { Component } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import {Router} from "@angular/router";
import {TranslateService} from "@ngx-translate/core";

@Component({
  selector: 'app-guest-layout',
  templateUrl: './guest-layout.component.html',
  styleUrls: ['./guest-layout.component.css']
})
export class GuestLayoutComponent {

  public languages = ["EN", "SK", "CZ"]
  public selectedLanguage = "EN";

  constructor(
    private router: Router,
    private translate: TranslateService
  ) {
  }

  goTo(path: string) {
    this.router.navigate([path]);
  }

  selectLanguage(language: string) {
    this.selectedLanguage = language;
    this.translate.use(language);
    console.log("Selected language: " + language)
  }

}
