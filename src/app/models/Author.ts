import {Book} from "./Book";

export class Author {
  constructor(
    public id: number,
    public author_first_name: string,
    public author_last_name: string,
  ) {
  }
}
