export class Book {
  constructor(
    public title: string,
    public isbn: string,
    public genres: string[],
    public publishYear: number,
    public authors: string[],
    public _id?: string,
  ) {
  }
}
