import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {SearchResultsComponent} from "./guest/search-results/search-results.component";
import {LoginComponent} from "./guest/login/login.component";
import {AdminLayoutComponent} from "./admin/admin-layout/admin-layout.component";
import {GuestLayoutComponent} from "./guest/guest-layout/guest-layout.component";
import {WelcomeComponent} from "./admin/welcome/welcome.component";
import {AddBookComponent} from "./admin/add-book/add-book.component";
import {AuthGuardService} from "./services/auth-guard.service";
import {BooksComponent} from "./admin/books/books.component";

// toto sa generuje do app.component.html
const routes: Routes = [
  {
    path: "",
    component: GuestLayoutComponent,
    children: [
      { path: "" , component: SearchResultsComponent},
      { path: "login", component: LoginComponent}
    ]
  },
  {
    path: "admin",
    component: AdminLayoutComponent,
    canActivate: [AuthGuardService],
    children: [
      { path: "", component: WelcomeComponent},
      { path: "books", component: BooksComponent},
      { path: "add", component: AddBookComponent},
      { path: "edit/:id", component: AddBookComponent},
    ]
  },
  // { path: "**", component: PageNotFoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
