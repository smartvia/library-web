import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {User} from "../../models/User";
import {Observable} from "rxjs";
import {BookService} from "../../services/book.service";
import {Book} from "../../models/Book";

@Component({
  selector: 'app-borrow',
  templateUrl: './borrow.component.html',
  styleUrls: ['./borrow.component.css']
})
export class BorrowComponent implements OnInit {
  public selectedUser: User;
  public users$: Observable<User[]>;

  @Input() bookId: string
  @Input() borrowed: boolean

  @Output() onBookAction = new EventEmitter<string>();

  constructor(private bookService: BookService) { }

  ngOnInit(): void {
    this.users$ = this.bookService.getUsers()
  }

  onSelect(user: User) {
    console.log("Borrowing ", user.id, " to ", this.bookId)
    this.bookService.lendBook(user.id, this.bookId);
    this.onBookAction.emit()

  }
  onReturn() {
    console.log("Book returned")
    this.bookService.returnBook(this.bookId);
    this.onBookAction.emit()
  }
}
