import { Component, OnInit } from '@angular/core';
import {Book} from "../../models/Book";
import {BookService} from "../../services/book.service";
import {User} from "../../models/User";
import {Observable, Subscription} from "rxjs";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-add-book',
  templateUrl: './add-book.component.html',
  styleUrls: ['./add-book.component.css']
})
export class AddBookComponent implements OnInit {
  public title = "";
  public isbn = "" ;
  public publishYear = "";
  public genres = "";
  public authors = "";

  public publishYearError = false;
  public emptyInputError = false;

  public editedBook: Book;
  private subscription: Subscription;

  constructor(
    private bookService: BookService,
    private router: Router,
    private route: ActivatedRoute
    ) { }

  ngOnInit(): void {
    this.subscription = this.route.params.subscribe(params => {
      // id specified in app-routing.moduele.ts
      let bookId = params["id"]
      if (bookId) {
        this.bookService.getBook(bookId).subscribe(book => {
          this.editedBook = book;

          this.title = book.title;
          this.isbn = book.isbn;
          this.publishYear = String(book.publishYear);
          this.authors = book.authors.join(", ");
          this.genres = book.genres.join(", ");

        })
      }
    })
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  createBookForService(): Book | undefined {
    this.emptyInputError = false;
    this.publishYearError = false;
    if (!this.title || !this.isbn || !this.genres || !this.publishYear || !this.authors) {
      this.emptyInputError = true;
    } else if (!Number(this.publishYear) || Number(this.publishYear)<=0) {
      this.publishYearError = true;
    } else {
      let book = new Book(
        this.title,
        this.isbn,
        this.genres.split(","),
        Number(this.publishYear),
        this.authors.split(","),
      )
      if (this.editedBook) {
        book._id = this.editedBook._id;
      }
      console.log("Adding New Book")
      console.log(book)
      return book;
    }
    return undefined;
  }

  addNewBook() {
    let book = this.createBookForService();
    if(book) {
      this.bookService.addBook(book).subscribe((res) => {
          console.log("New Book Created");
          console.log(res);
          this.router.navigate(['/admin/books'])
        },
        (error) => {
          console.log(error);
        });
    }
  }

  editBook() {
    let book = this.createBookForService();
    if(book) {
      this.bookService.updateBook(book).subscribe((res) => {
          console.log("Book Updated");
          console.log(res);
          this.router.navigate(['/admin/books'])
        },
        (error) => {
          console.log(error);
        });
    }
  }
}
