import { Component, OnInit } from '@angular/core';
import {Book} from "../../models/Book";
import {BookService} from "../../services/book.service";
import {Router} from "@angular/router";
import {Borrowing} from "../../models/Borrowing";

@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css']
})
export class BooksComponent implements OnInit {
  public displayedColumns: string[] = [/*'id',*/ 'title', 'authors'/*, 'genres', 'isbn', 'publishYear'*/, 'borrow', 'actions'];
  public books: Book[];
  public borrowings: Borrowing[];

  constructor(
    private bookService: BookService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.loadData();
  }
  loadData() {
    this.bookService.getBooks("").subscribe(booksFromService => {
      this.books = booksFromService
    })
    this.bookService.getBorrowings().subscribe(borrowingsFromService => {
      this.borrowings = borrowingsFromService
    })
  }
  deleteBook(book: Book) {
    if (confirm("Would you like to delete '" + book.title + "' book?"))
    if (book._id) {
      this.bookService.deleteBook(book._id).subscribe(res => {
        this.loadData()
      })
    }
  }

  editBook(book: Book) {
    if (book._id) {
      this.router.navigate(["/admin/edit", book._id])
    }
  }
  isBorrowed(book: Book) {
    if (book._id) {
      return this.borrowings.some(
        borrowing => (book._id == borrowing.bookId/* && borrowing.status ===""*/)
      )
    }
    return false;
    // return book.available
  }


}
