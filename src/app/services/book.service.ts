import { Injectable } from '@angular/core';
import { Book } from '../models/Book';
import {HttpClient, HttpResponse} from "@angular/common/http";
import {map, tap} from "rxjs/operators";
import {GlobalStorageService} from "./global-storage.service";
import {Author} from "../models/Author";
import {User} from "../models/User";
import {Observable, of} from "rxjs";
import {Borrowing} from "../models/Borrowing";

@Injectable({
  providedIn: 'root'
})
export class BookService {

  private api = "http://dev.smartvia.sk/api";
  private apiSearchUrl = this.api + "/books?q=";
  private apiLoginUrl = this.api + "/users/token";
  private apiAddBookUrl =  this.api + "/books";
  private apiAuthorsUrl =  this.api + "/authors";
  private apiUsersUrl =  this.api + "/users";
  private apiBookUrl = (id: string) => this.api + "/books/" + id;
  private apiLendUrl = (userId: string, bookId: string) =>
    this.api + "/users/" + userId + "/books/" + bookId;
  private apiBorrowingsUrl = this.api + "/borrowings";

  constructor(
    private http: HttpClient,
    private globalStorageService: GlobalStorageService
  ) { }

  // This service method is only for Python users
  getAuthors() {
    return this.http.get<Author>(this.apiAuthorsUrl);
  }


  private users: User[];
  private inProgress = false;
  private waiters: any = [];

  getUsers(): Observable<User[]> {
    // let users = [
    //   new User("3241", "Ivan"),
    //   new User("1423123", "Zuzana"),
    //   new User("423", "Fero"),
    // ];
    // return of(users);
    if (this.users) {
      return of(this.users);
    } else if (this.inProgress) {
      return new Observable<User[]>(subscriber => {
        this.waiters.push((users: User[]) => {
          subscriber.next(users)
          subscriber.complete()
          })
        })
    } else {
      this.inProgress = true;
      return new Observable(subscriber => {
        this.http.get(
          this.apiUsersUrl,
          this.optionsWithToken()
        ).pipe(map(this.remoteUsers)).subscribe(users => {
          this.users = users;
          this.waiters.forEach((waiter: any) => waiter(users));
          subscriber.next(users);
          subscriber.complete();
        })
      });
    }
  }

  optionsWithToken(){
    return {
      headers: {
        "authorization": "Bearer " + this.globalStorageService.getToken()
      }
    }
  }

  getBooks(query: string) {
    return this.http.get(this.apiSearchUrl + query).pipe(map(this.remoteBooks));
  }

  login(login: string, password: string) {

    // Python && Java
    // {username: login, password: password},

    return this.http.post(
      this.apiLoginUrl,
      {email: login, password: password},
      {
        observe: "response",
      }
    ).pipe(
      map(this.tokenExtraction)
    )
  }

  tokenExtraction(res: HttpResponse<any>): string|undefined {
    console.log(res)
    let token = res.body.token;

    //Python
    // let token = res.body.access_token;

    // Java
    // let token = res.headers.get('authorization');
    // token = token.replace("Bearer ", "")
    if (token) {
      return token;
    } else {
      return undefined;
    }
  }

  remoteBooks(res: any): Book[] {
    let books: Book[] = [];

    if (res.success) {
      for(let book of res.data) {
        books.push(new Book(book.title, book.isbn, book.genres, book.publishYear, book.authors, book._id))
      }
    }
    return books;
  }

  remoteUsers(res: any): User[] {
    let users: User[] = [];

    if (res.success) {
      for(let user of res.data) {
        users.push(new User(user._id, user.name))
      }
    }
    return users;
  }

  addBook(book: Book) {
    // let mappedBook = {
    //   title: book.title
    //   author: book.someDifferentAuthorProperty
    //
    // }

    return this.http.post(
      this.apiAddBookUrl,
      book, //mappedBook
      this.optionsWithToken()
    )
    // globalStorageService

  }

  deleteBook(_id: string) {
    return this.http.delete(
      this.apiBookUrl(_id),
      this.optionsWithToken()
    )
  }

  updateBook(book: Book) {
    if(book._id) {
      return this.http.put(
        this.apiBookUrl(book._id),
        book,
        this.optionsWithToken()
      )
    }
    throw new Error("No book ID");
  }

  getBook(id: string) {
    return this.http.get(
      this.apiBookUrl(id)
    ).pipe(map(this.remoteBookMapping))
  }
  remoteBookMapping(res: any): Book {
    return res.data;
  }
  private borrowings: Borrowing[] = [];
  getBorrowings() {
    // Fake Borrowings
    return of(this.borrowings);
    // return this.http.get(
    //   this.apiBorrowingsUrl,
    //   this.optionsWithToken()
    // )
  }

  lendBook(userId: string, bookId: string) {
    this.borrowings.push(new Borrowing(bookId, userId));
    //   return this.http.put(
    //     this.apiLendUrl(userId, bookId),
    //     {
    //       action: "lend,
    //     },
    //     this.optionsWithToken()
    //   )
  }

  returnBook(bookId: string) {
    this.borrowings = this.borrowings.filter(borrowing => borrowing.bookId != bookId);
    // TODO how to get userId?
    //   return this.http.put(
    //     this.apiLendUrl(userId, bookId),
    //     {
    //       action: "return,
    //     },
    //     this.optionsWithToken()
    //   )
  }

  // }


}
