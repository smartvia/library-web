import { Injectable } from '@angular/core';
import {CanActivate, Router} from "@angular/router";
import {GlobalStorageService} from "./global-storage.service";

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate {
  constructor(
    private globalStorageService: GlobalStorageService,
    private router: Router
  ) {}
  canActivate(): boolean {
    if (!this.globalStorageService.getToken()) {
      this.router.navigate(['/login']);
      return false;
    }
    return true;
  }
}
