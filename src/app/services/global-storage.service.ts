import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class GlobalStorageService {

  private token: string | undefined;

  constructor() {
    let localStorageToken = localStorage.getItem("token");
    if (localStorageToken) {
      this.token = localStorageToken;
    }
  }

  logout() {
    this.token = undefined;
    localStorage.removeItem('token');
  }

  login(token: string) {
    this.token = token;
    localStorage.setItem("token", token);
  }

  getToken() {
    return this.token;
  }

  add(a: number, b: number){
    return 7;
  }
}
