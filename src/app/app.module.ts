import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { GuestLayoutComponent } from './guest/guest-layout/guest-layout.component';
import {MatMenuModule} from '@angular/material/menu';
import {BookService} from "./services/book.service";
import {HttpClientModule} from "@angular/common/http";
import { SearchResultsComponent } from './guest/search-results/search-results.component';
import {MatCardModule} from '@angular/material/card';
import {MatInputModule} from '@angular/material/input';
import {FormsModule} from "@angular/forms";
import { LoginComponent } from './guest/login/login.component';
import {GlobalStorageService} from "./services/global-storage.service";
import { AdminLayoutComponent } from './admin/admin-layout/admin-layout.component';
import { WelcomeComponent } from './admin/welcome/welcome.component';
import { AddBookComponent } from './admin/add-book/add-book.component';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatSelectModule} from '@angular/material/select';
import { BooksComponent } from './admin/books/books.component';
import {MatTableModule} from '@angular/material/table';
import { BorrowComponent } from './admin/borrow/borrow.component';
import {TranslateLoader, TranslateModule} from "@ngx-translate/core";
import {CustomLoader} from "./translation.loader";

@NgModule({
  declarations: [
    AppComponent,
    GuestLayoutComponent,
    SearchResultsComponent,
    LoginComponent,
    AdminLayoutComponent,
    WelcomeComponent,
    AddBookComponent,
    BooksComponent,
    BorrowComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    BrowserAnimationsModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useClass: CustomLoader,
      }
    }),
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatMenuModule,
    MatCardModule,
    MatInputModule,
    MatAutocompleteModule,
    MatSelectModule,
    MatTableModule
  ],
  providers: [BookService, GlobalStorageService],
  bootstrap: [AppComponent]
})
export class AppModule { }
